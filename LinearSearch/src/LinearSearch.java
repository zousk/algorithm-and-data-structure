public class LinearSearch {

    // LinearSearch构造函数设置为私有函数，在类内部仍然可以使用构造函数，外部不能使用
    private LinearSearch() {}

    public static void main(String[] args) {
        Integer[] data = ArrayGenerator.generateOrderedArray(100000000);
        Integer target = -1;
        // 起始时间
        long startTime = System.nanoTime();
        int searchRet = LinearSearch.search(data, target);
        long endTime = System.nanoTime();
        double duringTime = (endTime - startTime) / 1000000000.0;
        System.out.println("during time is " + duringTime);

        // student类测试
        // Student[] students = {new Student("hello"), new Student("world"), new Student("jelly")};
        // Student targetStud = new Student("Jelly");
        // int searchStud = LinearSearch.search(students, targetStud);
        // System.out.println(searchStud);
    }

    // search方法
    public static <E> int search(E[] data, E target) {
        for (int index = 0; index < data.length; index++) {
            if (data[index].equals(target)) {
                return index;
            }
        }
        return -1;
    }
}
