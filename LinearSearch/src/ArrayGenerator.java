public class ArrayGenerator {
    private ArrayGenerator() {}

    // 生成数组
    public static Integer[] generateOrderedArray(int n) {
        Integer[] generateArray = new Integer[n];
        for (int index=0; index<n; index++) {
            generateArray[index] = index;
        }
        return generateArray;
    }
}
