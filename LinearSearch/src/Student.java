public class Student {

    private String name;

    public Student(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object student) {

        // 类型判断
        if (this == student) {
            return true;
        }

        if (student == null) {
            return false;
        }

        if (this.getClass() != student.getClass()) {
            return false;
        }

        Student tempStudent = (Student)student;
        return this.name.equalsIgnoreCase(tempStudent.name);
    }
}
