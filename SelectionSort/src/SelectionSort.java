import java.util.Arrays;

public class SelectionSort {

    private SelectionSort() {}

    private static void swap(int[] array, int minIndex, int index) {
        int tempValue = array[index];
        array[index] = array[minIndex];
        array[minIndex] = tempValue;
    }

    public static void sort(int[] array) {
        // 原地选择排序算法
        for (int index=0; index < array.length; index ++) {
            if (index == array.length - 1) {
                // 设置循环终止条件
                break;
            }
            int minIndex = index;
            for (int innerIndex=minIndex; innerIndex < array.length; innerIndex++) {
                if (array[innerIndex] < array[index]) {
                    minIndex = innerIndex;
                }
            }
            // 和最小值交换位置(static方法中调用的方法也应该是静态方法)
            SelectionSort.swap(array, minIndex, index);
        }
    }

    public static void main(String[] args) {
        int[] array = {1, 4, 2, 3, 6, 5, 9, 7};
        SelectionSort.sort(array);
        System.out.println(Arrays.toString(array));
    }
}
